# A Drupal App Inside an Ugly Docker

## 1) CASO DE ESTUDIO
### 1.1) Confirmemos la rama de nuestro repositorio
```bash
git checkout master
```

### 1.2) Construyamos nuestra imagen actual
```bash
docker build -t dockeruio/exercise:v1 --network=host .
```
### 1.2.*) Por motivos de tiempo hemos construido la imagen y la hemos subido a Docker hub, así que procedemos con el siguiente paso

### 1.3) Creemos una red para nuestro ambiente
```bash
docker network create --driver bridge dcuio_network
```
### 1.4) Restauremos el sistema de archivos de PostgreSQL
```bash
sh recover-db.sh
```

### 1.5) Creemos una variable de ambiente para manejar nuestra contraseña de BDD
```bash
export DB_PASSWORD=your_postgres_password
```


### 1.6) Subamos nuestro contenedor
```bash
docker run --rm \
  -it \
  --name exercise \
  -p 80:80 \
  --env POSTGRES_PASSWORD=$DB_PASSWORD \
  --mount type=bind,source="$(pwd)"/drupal_modules,target=/var/www/html/modules \
  --mount type=bind,source="$(pwd)"/drupal_profiles,target=/var/www/html/profiles \
  --mount type=bind,source="$(pwd)"/drupal_themes,target=/var/www/html/themes \
  --mount type=bind,source="$(pwd)"/drupal_sites,target=/var/www/html/sites \
  --mount type=bind,source="$(pwd)"/db_data,target=/var/lib/postgresql/data \
  --network dcuio_network \
  --hostname exercise-postgres \
  dockeruio/exercise:v1
```
### 1.6.w) Los sub pasos de este numeral están diseñados para Docker Tool Box (Windows)
### 1.6.w.1) Creemos un volumen para que la base pueda subir correctamente
```bash
docker volume create windows_exercise_named_volume
```

### 1.6.w.2) Cambiémonos temporalmente a la rama paso-3
```bash
git checkout paso-3
```

### 1.6.w.3) Restauremos el respaldo de base de datos en nuestro volumen
```bash
docker run -it --rm \
    --mount type=volume,source=windows_exercise_named_volume,target=/var/lib/postgresql/data \
    --mount type=bind,source="$(pwd)"/backup,target=/backup alpine \
    sh -c "rm -rf /var/lib/postgresql/data/* ; tar -C /var/lib/postgresql/data/ -xjvf /backup/db_backup.tar.bz2"
```

### 1.6.w.4) Subamos nuestro contenedor
```bash
docker run --rm \
  -it \
  --name exercise \
  -p 80:80 \
  --env POSTGRES_PASSWORD=$DB_PASSWORD \
  --mount type=bind,source="$(pwd)"/drupal_modules,target=/var/www/html/modules \
  --mount type=bind,source="$(pwd)"/drupal_profiles,target=/var/www/html/profiles \
  --mount type=bind,source="$(pwd)"/drupal_themes,target=/var/www/html/themes \
  --mount type=bind,source="$(pwd)"/drupal_sites,target=/var/www/html/sites \
  --mount type=volume,source=windows_exercise_named_volume,target=/var/lib/postgresql/data \
  --network dcuio_network \
  --hostname exercise-postgres \
  dockeruio/exercise:v1
```
### 1.6.w.5) Regresemos a la rama master de nuestro repositorio
```bash
git checkout master
```

### 1.7) Verifiquemos el estado de nuestro servicio
Abramos en nuestro navegador favorito [http://localhost](http://localhost)

### 1.7.w) Verifiquemos el estado de nuestro servicio en Docker Tool Box (Windows)
Abramos en nuestro navegador favorito [http://<<IP_MAQUINA_VIRTUAL>>](http://<<IP_MAQUINA_VIRTUAL>>)

### 1.8) Apaguemos el servicio
Ctrl+C

## 2) IMÁGENES PEQUEÑAS
### 2.1) Confirmemos la rama de nuestro repositorio
```bash
git checkout paso-2
```
### 2.2) Creemos imágenes pequeñas y construyamos

### 2.2.1.a) Generemos la imagen de apache/php

* inicio: 1
* fin: 283
* archivo: Dockerfile-apache

#### 2.2.1.b) Instrucciones adicionales:
Descomentar última línea [CMD]

#### 2.2.1.c) Construyamos la imagen:
```bash
docker build -t dockeruio/exercise-apache:v1 --network=host -f Dockerfile-apache .
```
### 2.2.1.c.*) Por motivos de tiempo hemos construido la imagen y la hemos subido a Docker hub, así que procedemos con el siguiente paso

### 2.2.2.a) Generemos la imagen de drupal

* inicio: 284
* fin: 349
* archivo: Dockerfile-drupal

#### 2.2.2.b) Instrucciones adicionales:
Indicar imagen base [FROM dockeruio/exercise-apache:v1]

#### 2.2.2.c) Construyamos la imagen:
```bash
docker build -t dockeruio/exercise-drupal:v1 --network=host -f Dockerfile-drupal .
```
### 2.2.2.c.*) Por motivos de tiempo hemos construido la imagen y la hemos subido a Docker hub, así que procedemos con el siguiente paso

### 2.2.3.a) Generemos la imagen de postgreSQL

* inicio: 350
* fin: 524
* archivo: Dockerfile-postgres

#### 2.2.3.b) Instrucciones adicionales:
Indicar imagen base [FROM debian:stretch-slim]

Descomentar 3 últimas líneas [ENTRYPOINT, EXPOSE, CMD]

#### 2.2.2.c) Construyamos la imagen:
```bash
docker build -t dockeruio/exercise-postgres:v1 --network=host -f Dockerfile-postgres .
```
### 2.2.3.c.*) Por motivos de tiempo hemos construido la imagen y la hemos subido a Docker hub, así que procedemos con el siguiente paso

### 2.3) Corramos nuestras imágenes
#### 2.3.1) PostgreSQL
```bash
docker run --rm \
  -it \
  --name exercise-postgres \
  --env POSTGRES_PASSWORD=$DB_PASSWORD \
  --mount type=bind,source="$(pwd)"/db_data,target=/var/lib/postgresql/data \
  --network dcuio_network \
  dockeruio/exercise-postgres:v1
```
### 2.3.1.w) Ejecución PostgreSQL en Docker Tool Box (Windows)
```bash
docker run --rm \
  -it \
  --name exercise-postgres \
  --env POSTGRES_PASSWORD=$DB_PASSWORD \
  --mount type=volume,source=windows_exercise_named_volume,target=/var/lib/postgresql/data \
  --network dcuio_network \
  dockeruio/exercise-postgres:v1
```
#### 2.3.2) Drupal
```bash
docker run --rm \
  -it \
  --name exercise-drupal \
  -p 80:80 \
  --mount type=bind,source="$(pwd)"/drupal_modules,target=/var/www/html/modules \
  --mount type=bind,source="$(pwd)"/drupal_profiles,target=/var/www/html/profiles \
  --mount type=bind,source="$(pwd)"/drupal_themes,target=/var/www/html/themes \
  --mount type=bind,source="$(pwd)"/drupal_sites,target=/var/www/html/sites \
  --network dcuio_network \
  dockeruio/exercise-drupal:v1
```

### 2.4) Verifiquemos el estado de nuestro servicio
Abramos en nuestro navegador favorito [http://localhost](http://localhost)

### 2.4.w) Verifiquemos el estado de nuestro servicio en Docker Tool Box (Windows)
Abramos en nuestro navegador favorito [http://<<IP_MAQUINA_VIRTUAL>>](http://<<IP_MAQUINA_VIRTUAL>>)

### 2.5) Apaguemos nuestros contenedores con
Ctrl+C

## 3) VOLÚMENES NOMBRADOS
### 3.1) Confirmemos la rama de nuestro repositorio
```bash
git checkout paso-3
```

### 3.2) Creemos un comprimido tipo tar de nuestros archivos de base de datos 
```bash
mkdir backup
docker run -it --rm --mount type=bind,source="$(pwd)"/db_data,target=/volume \
    --mount type=bind,source="$(pwd)"/backup,target=/backup alpine \
    tar -cjvf /backup/db_backup.tar.bz2 -C /volume ./
```
### 3.3) Creemos nuestro volumen nombrado
```bash
docker volume create exercise_named_volume
```
### 3.4) Restauremos el respaldo realizado en nuestro volumen nombrado
```bash
docker run -it --rm \
    --mount type=volume,source=exercise_named_volume,target=/var/lib/postgresql/data \
    --mount type=bind,source="$(pwd)"/backup,target=/backup alpine \
    sh -c "rm -rf /var/lib/postgresql/data/* ; tar -C /var/lib/postgresql/data/ -xjvf /backup/db_backup.tar.bz2"
```
### 3.5) Para confirmar que no estamos usando el directorio de nuestro proyecto lo renombraremos
```bash
mv db_data old_db_data
```
### 3.6) Corramos nuestra base de datos
```bash
docker run --rm \
  -it \
  --name exercise-postgres \
  --env POSTGRES_PASSWORD=$DB_PASSWORD \
  --mount type=volume,source=exercise_named_volume,target=/var/lib/postgresql/data \
  --network dcuio_network \
  dockeruio/exercise-postgres:v1
```
### 3.7) Corramos nuestro servidor de aplicaciones
```bash
docker run --rm \
  -it \
  --name exercise-drupal \
  -p 80:80 \
  --mount type=bind,source="$(pwd)"/drupal_modules,target=/var/www/html/modules \
  --mount type=bind,source="$(pwd)"/drupal_profiles,target=/var/www/html/profiles \
  --mount type=bind,source="$(pwd)"/drupal_themes,target=/var/www/html/themes \
  --mount type=bind,source="$(pwd)"/drupal_sites,target=/var/www/html/sites \
  --network dcuio_network \
  dockeruio/exercise-drupal:v1
```
### 3.8) Verifiquemos el estado del volumen (sección mounts)
```bash
docker inspect exercise-postgres
```
### 3.9) Verifiquemos el estado de nuestro servicio
Abramos en nuestro navegador favorito [http://localhost](http://localhost)

### 3.9.w) Verifiquemos el estado de nuestro servicio en Docker Tool Box (Windows)
Abramos en nuestro navegador favorito [http://<<IP_MAQUINA_VIRTUAL>>](http://<<IP_MAQUINA_VIRTUAL>>)

### 3.10) Apaguemos nuestros contenedores con
Ctrl+C

## 4) MINI CAPAS
### 4.1) Confirmemos la rama de nuestro repositorio
```bash
git checkout paso-4
```

### 4.2) Modifiquemos nuestros archivos de imagen reutilizando imágenes disponibles
### 4.2.1.a) Imagen drupal
```dockerfile
FROM drupal:8.6.9
```
### 4.2.1.b) Construyamos la imagen
```bash
docker build -t dockeruio/exercise-drupal:v1.1 --network=host -f Dockerfile-mini-drupal .
```
### 4.2.1.b.*) Por motivos de tiempo hemos construido la imagen y la hemos subido a Docker hub, así que procedemos con el siguiente paso

### 4.2.2.a) Imagen PostgreSQL:
```dockerfile
FROM postgres:9.6.12
```
### 4.2.2.b) Construyamos la imagen
```bash
docker build -t dockeruio/exercise-postgres:v1.1 --network=host -f Dockerfile-mini-postgres .
```
### 4.2.2.b.*) Por motivos de tiempo hemos construido la imagen y la hemos subido a Docker hub, así que procedemos con el siguiente paso


### 4.3) Corramos nuestra base de datos
No hay necesidad de utilizar la capa generada para la BDD, utilicemos la imagen original directamente
```bash
docker run --rm \
  -it \
  --name exercise-postgres \
  --env POSTGRES_PASSWORD=$DB_PASSWORD \
  --mount type=volume,source=exercise_named_volume,target=/var/lib/postgresql/data \
  --network dcuio_network \
  postgres:9.6.12
```

### 4.4) Corramos nuestro servidor de aplicaciones
```bash
docker run --rm \
  -it \
  --name exercise-drupal \
  -p 80:80 \
  --mount type=bind,source="$(pwd)"/drupal_modules,target=/var/www/html/modules \
  --mount type=bind,source="$(pwd)"/drupal_profiles,target=/var/www/html/profiles \
  --mount type=bind,source="$(pwd)"/drupal_themes,target=/var/www/html/themes \
  --mount type=bind,source="$(pwd)"/drupal_sites,target=/var/www/html/sites \
  --network dcuio_network \
  dockeruio/exercise-drupal:v1.1
```
### 4.5) Verifiquemos el estado de nuestro servicio
Abramos en nuestro navegador favorito [http://localhost](http://localhost)

### 4.5.w) Verifiquemos el estado de nuestro servicio en Docker Tool Box (Windows)
Abramos en nuestro navegador favorito [http://<<IP_MAQUINA_VIRTUAL>>](http://<<IP_MAQUINA_VIRTUAL>>)

### 4.6) Apaguemos nuestros contenedores con
Ctrl+C

## 5) GENERAR APP IMÁGENES
### 5.1) Confirmemos la rama de nuestro repositorio
```bash
git checkout paso-5
```

### 5.2.1) Extendemos la imagen oficial de drupal con el contenido específico de nuestro sitio/app
Agregamos las instrucciones de copiado de nuestra app a la imagen
```dockerfile
COPY drupal_modules/ /var/www/html/modules/
COPY drupal_profiles/ /var/www/html/profiles/
COPY drupal_themes/ /var/www/html/themes/
COPY drupal_sites/ /var/www/html/sites/
```
### 5.2.2) Compilamos nuevamente la imagen de nuestra aplicación drupal
```bash
docker build -t dockeruio/exercise-drupal:v1.2 --network=host -f Dockerfile-mini-drupal .
```
### 5.2.2.*) Por motivos de tiempo hemos construido la imagen y la hemos subido a Docker hub, así que procedemos con el siguiente paso

### 5.3) Ejecutamos nuestros contenedores nuevamente
### 5.3.1) Postgres
```bash
docker run --rm \
  -it \
  --name exercise-postgres \
  --env POSTGRES_PASSWORD=$DB_PASSWORD \
  --mount type=volume,source=exercise_named_volume,target=/var/lib/postgresql/data \
  --network dcuio_network \
  postgres:9.6.12
```
### 5.3.2) Drupal
```bash
docker run --rm \
  -it \
  --name exercise-drupal \
  -p 80:80 \
  --network dcuio_network \
  dockeruio/exercise-drupal:v1.2
```
### 5.4) Verifiquemos el estado de nuestro servicio
Abramos en nuestro navegador favorito [http://localhost](http://localhost)

### 5.4.w) Verifiquemos el estado de nuestro servicio en Docker Tool Box (Windows)
Abramos en nuestro navegador favorito [http://<<IP_MAQUINA_VIRTUAL>>](http://<<IP_MAQUINA_VIRTUAL>>)

### 5.5) Apaguemos nuestros contenedores con
Ctrl+C

## 6) ETIQUETADO
### 6.1) Confirmemos la rama de nuestro repositorio
```bash
git checkout paso-6
```

### 6.2) Agreguemos una etiqueta a nuestra imagen para identificarla como un artefacto que se encuentra en el ambiente de pruebas
```bash
docker tag dockeruio/exercise-drupal:v1.2 dockeruio/exercise-drupal:v1.2.qa
```
### 6.3) Publiquemos esta imagen en el registro
### 6.3.1) Iniciamos sesión
```bash
docker login
```
### 6.3.1) Publicamos nuestro artefacto
```bash
docker push dockeruio/exercise-drupal:v1.2.qa
```
## 7) AGRUPANDO EN COMPOSICIÓN
### 7.1) Confirmemos la rama de nuestro repositorio
```bash
git checkout paso-7
```

### 7.2) Generemos nuestro archivo de composición
docker-compose.yml

### 7.3) Arrancamos nuestro compose
```bash
docker-compose up
```
### 7.4) Verifiquemos el estado de nuestro servicio
Abramos en nuestro navegador favorito [http://localhost](http://localhost)

### 7.4.w) Verifiquemos el estado de nuestro servicio en Docker Tool Box (Windows)
Abramos en nuestro navegador favorito [http://<<IP_MAQUINA_VIRTUAL>>](http://<<IP_MAQUINA_VIRTUAL>>)

### 7.5) Apaguemos nuestros contenedores con
Ctrl+C

## Comandos Complementarios
Limpiar archivos no pertenecientes al repositorio
```bash
git clean -fd
```
Verificar el estado de la red
```bash
docker network inspect dcuio_network
```
Limpiar todas las imágenes
```bash
docker rmi $(docker images -a -q)
```
Borrar todos los contenedores
```bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
```
Borrar un volumen en específico
```bash
docker volume rm exercise_named_volume
```
