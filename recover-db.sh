#!/bin/bash

cd db_data
mkdir pg_tblspc
mkdir pg_replslot
mkdir pg_snapshots
mkdir pg_twophase
mkdir pg_logical
cd pg_logical
mkdir snapshots
mkdir mappings
cd ..
mkdir pg_commit_ts
mkdir pg_stat_tmp
mkdir pg_xlog
cd pg_xlog
mkdir archive_status
cd ..
mkdir pg_stat
