#!/bin/bash

cd db_data && rm -rf *
cd ../drupal_modules && rm -rf *
cd ../drupal_profiles && rm -rf *
cd ../drupal_sites && rm -rf *
cd ../drupal_themes && rm -rf *
